<h1 align="center" style="font-size:45px">待支付平台文档</h1>
<p align="center">
  <img src="./images/icon.png" style="width:130px" class="no-zoom" />
</p>

## 介绍
  此文档用于对`待支付平台`使用上的一些问题作为说明，会列举一些常遇到的问题以及遇到这些问题时的常用处理方式。

  当然可能有一些问题是我们没有考虑到的，可以联系我们的客服人员，或者在此文档右下角的聊天留言板（`OPEN CHAT`）中，说明问题。我们会在收到问题后的第一时间回复。

> APP下载链接：[苹果]() [安卓]()

## 目录

* [APP](/app/app)
  * [APP介绍](/app/app?id=app-介绍)
  * [操作指南](/app/appOperation)
    * [充值](/app/appOperation?id=充值)
    * [提现](/app/appOperation?id=提现)
      * [银行卡](/app/appOperation?id=银行卡)
      * [提现至银行卡](/app/appOperation?id=提现至银行卡)
    * [钱包](/app/appOperation?id=钱包)
    * [二维码](/app/appOperation?id=二维码)
  * [常见问题](/app/appProblem)
    * [抓单匹配不到](/app/appProblem?id=抓单匹配不到)
    * [充值后余额不变](/app/appProblem?id=充值后余额不变)
    * [提现后没收到钱](/app/appProblem?id=提现后没收到钱)
    * [提现时选择银行卡为空](/app/appProblem?id=提现时选择银行卡为空)
